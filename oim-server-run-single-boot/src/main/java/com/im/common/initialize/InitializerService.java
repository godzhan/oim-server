package com.im.common.initialize;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.im.server.general.common.bean.User;
import com.im.server.general.common.dao.UserDAO;
import com.im.server.general.common.data.system.UserInfoQuery;
import com.onlyxiahui.common.util.OnlyMD5Util;

@Service
@Transactional
public class InitializerService {
	
	@Resource
	UserDAO userDAO;
	
	public void initialize() {
		initRootUser();
	}
	
	/**
	 * 初始化超级管理员
	 */
	private void initRootUser(){
		UserInfoQuery userQuery=new UserInfoQuery();
		userQuery.setType(User.type_root);
		List<User> list = userDAO.queryList(userQuery, null, User.class);
		if(list.isEmpty()){
			User u=new User();
			u.setNumber(-1);
			u.setAccount("root");
			u.setPassword(OnlyMD5Util.md5L32("123456"));
			u.setCreateTime(new Date());
			u.setName("超级管理员");
			u.setFlag("1");
			u.setType(User.type_root);
			
			userDAO.save(u);
		}
	}

}

import service from '@/libs/service';

let user = {};

user.list = function (query, page, back) {
    var body = {
        'userQuery': query,
        'page': page
    };
    service.postBody('/manage/core/user/list', body, back);
};

user.addOrUpdate = function (bean, back) {
    var body = {'user': bean};
    service.postBody('/manage/core/user/addOrUpdate', body, back);
};

user.get = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/core/user/get', body, back);
};

user.delete = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/core/user/delete', body, back);
};

user.isExist = function (id, account, back) {
    var body = {
        'id': id,
        'account': account
    };
    service.postBody('/manage/core/user/isExist', body, back);
};

user.getInfo = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/core/user/getInfo', body, back);
};

user.updatePassword = function (id, password, back) {
    var body = {
        'id': id,
        'password': password
    };
    service.postBody('/manage/core/user/updatePassword', body, back);
};

user.toAdmin = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/core/user/toAdmin', body, back);
};

export default user;
